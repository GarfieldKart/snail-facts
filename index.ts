import * as fs from 'fs';
import remove from 'confusables';
import { Client } from 'hiven';
import sync from 'sync-request'

const token = process.env["token"]
const facts = loadFacts()
const client = new Client({ type: 'user' });
client.on('init', () => {
    console.log(`Ready, connected as ${client.user.username} (${client.user.id})`);
});

client.on('message', (msg) => {
    if (msg.author.id == client.user.id) return;
    if (prefix(msg.content.toLocaleLowerCase())) {
        console.log(`MSG : [${msg.room.name ? msg.room.name : 'unamed'}:${msg.room.id}@${msg.house ? msg.house.name : 'DM'}:${msg.house ? msg.house.id : ''}] ${msg.author.username}: ${msg.content}`);
        msg.room.send("Snail fact: " + rand(facts))     
    }
});
  
client.connect(token)

function loadFacts(): JSON {
    return JSON.parse(sync('GET', 'https://gitlab.com/GarfieldKart/snail-facts/-/raw/master/facts.json').body.toString())
}
function prefix(str: string): boolean {
    return remove(str).startsWith("!snail") || remove(str).startsWith("!snaii")
}
function rand(items): any {
    return items[items.length * Math.random() | 0];
}